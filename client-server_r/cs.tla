--------------------------------- MODULE cs ---------------------------------

CONSTANT Client, Server

VARIABLE link, semaphore

TypeOK ==
  /\ link \subseteq (Client \X Server)
  /\ semaphore \subseteq Server

Init ==
  /\ link = {}
  /\ semaphore = Server

Connect(c, s) ==
  /\ s \in semaphore
  /\ link' = link \union {<<c, s>>}
  /\ semaphore' = semaphore \ {s}

Disconnect(c, s) ==
  /\ <<c, s>> \in link
  /\ link' = link \ {<<c, s>>}
  /\ semaphore' = semaphore \union {s}

Next ==
  \E c \in Client, s \in Server :
    \/ Connect(c, s)
    \/ Disconnect(c, s)

Spec ==
  Init /\ [][Next]_<<link, semaphore>>



Unique ==
  \A ca, cb \in Client, s \in Server :
    (<<ca, s>> \in link /\ <<cb, s>> \in link) => ca = cb

No_Links ==
  \A c \in Client, s \in Server :
    /\ <<c, s>> \notin link
    /\ s \in semaphore

One_Link ==
  \E c0 \in Client, s0 \in Server :
    /\ <<c0, s0>> \in link
    /\ s0 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0) => s \in semaphore
      /\ (c /= c0 \/ s /= s0) => <<c, s>> \notin link

Two_Links_Same ==
  \E c0 \in Client, s0, s1 \in Server :
    /\ s0 /= s1
    /\ <<c0, s0>> \in link
    /\ <<c0, s1>> \in link
    /\ s0 \notin semaphore
    /\ s1 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0 /\ s /= s1) => s \in semaphore
      /\ ((c /= c0 \/ s /= s0) /\ (c /= c0 \/ s /= s1))
        => <<c, s>> \notin link

Two_Links_Diff ==
  \E c0, c1 \in Client, s0, s1 \in Server :
    /\ c0 /= c1 /\ s0 /= s1
    /\ <<c0, s1>> \in link
    /\ <<c1, s0>> \in link
    /\ s0 \notin semaphore
    /\ s1 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0 /\ s /= s1) => s \in semaphore
      /\ ((c /= c0 \/ s /= s1) /\ (c /= c1 \/ s /= s0))
        => <<c, s>> \notin link

Three_Links_Same ==
  \E c0 \in Client, s0, s1, s2 \in Server :
    /\ s0 /= s1 /\ s0 /= s2 /\ s1 /= s2
    /\ <<c0, s0>> \in link
    /\ <<c0, s1>> \in link
    /\ <<c0, s2>> \in link
    /\ s0 \notin semaphore
    /\ s1 \notin semaphore
    /\ s2 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0 /\ s /= s1 /\ s /= s2) => s \in semaphore
      /\ ((c /= c0 \/ s /= s0) /\ (c /= c0 \/ s /= s1) /\ (c /= c0 \/ s /= s2))
        => <<c, s>> \notin link

Three_Links_Half ==
  \E c0, c1 \in Client, s0, s1, s2 \in Server :
    /\ c0 /= c1 /\ s0 /= s1 /\ s0 /= s2 /\ s1 /= s2
    /\ <<c0, s1>> \in link
    /\ <<c0, s2>> \in link
    /\ <<c1, s0>> \in link
    /\ s0 \notin semaphore
    /\ s1 \notin semaphore
    /\ s2 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0 /\ s /= s1 /\ s /= s2) => s \in semaphore
      /\ ((c /= c0 \/ s /= s1) /\ (c /= c0 \/ s /= s2) /\ (c /= c1 \/ s /= s0))
        => <<c, s>> \notin link

Three_Links_Diff ==
  \E c0, c1, c2 \in Client, s0, s1, s2 \in Server :
    /\ c0 /= c1 /\ c0 /= c2 /\ c1 /= c2 /\ s0 /= s1 /\ s0 /= s2 /\ s1 /= s2
    /\ <<c0, s2>> \in link
    /\ <<c1, s1>> \in link
    /\ <<c2, s0>> \in link
    /\ s0 \notin semaphore
    /\ s1 \notin semaphore
    /\ s2 \notin semaphore
    /\ \A c \in Client, s \in Server :
      /\ (s /= s0 /\ s /= s1 /\ s /= s2) => s \in semaphore
      /\ ((c /= c0 \/ s /= s2) /\ (c /= c1 \/ s /= s1) /\ (c /= c2 \/ s /= s0))
        => <<c, s>> \notin link

Reachable ==
  \/ No_Links
  \/ One_Link
  \/ Two_Links_Same
  \/ Two_Links_Diff
  \/ Three_Links_Same
  \/ Three_Links_Half
  \/ Three_Links_Diff

=============================================================================
