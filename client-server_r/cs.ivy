#lang ivy1.7

after init  {
	semaphore(Y) := true ;
	link(X, Y) := false;
}

type client
type server

interpret client -> {0..2}
interpret server -> {0..2}

relation link(X: client, Y: server)
relation semaphore(X: server)

action connect(c: client, s: server) = {
	require semaphore(s);
	link(c, s) := true;
	semaphore(s) := false;
}

action disconnect(c: client, s: server) = {
	require link(c, s);
	link(c, s) := false;
	semaphore(s) := true;
}

export connect
export disconnect



invariant [unique] forall C1, C2 : client, S: server. link(C1, S) & link(C2, S) -> C1 = C2
# relation unique = forall C1, C2 : client, S: server. link(C1, S) & link(C2, S) -> C1 = C2

# invariant [help_semaphore_inv] forall S : server, C : client . link(C, S) -> ~semaphore(S)

# relation lcn(C:client) = forall S. ~link(C, S)
# relation lcsemaphore(C:client) = forall S. link(C, S)
# relation lcb(C:client) = (exists S. ~link(C, S)) & (exists S. link(C, S))
# relation lsn(S:server) = forall C. ~link(C, S)
# relation lssemaphore(S:server) = forall C. link(C, S)
# relation lsb(S:server) = (exists C. ~link(C, S)) & (exists C. link(C, S))
# relation sn(S:server) = ~semaphore(S)
# relation ssemaphore(S:server) = semaphore(S)
# 
# relation no_links_client = forall C. lcn(C)
# relation one_link_client = exists C. lcb(C) & forall C2. (C2 ~= C) -> lcn(C2)
# relation two_links_same_client = one_link_client
# relation two_links_diff_client = exists C. lcn(C) & forall C2. (C2 ~= C) -> lcb(C2)
# relation three_links_same_client = exists C. lcsemaphore(C) & forall C2. (C2 ~= C) -> lcn(C2)
# relation three_links_half_client = two_links_diff_client
# relation three_links_diff_client = forall C. lcb(C)
# 
# relation no_links_server = forall S. lsn(S) & ssemaphore(S)
# relation one_link_server = exists S. lsb(S) & sn(S) & forall S2. (S2 ~= S) -> (lsn(S2) & ssemaphore(S2))
# relation two_links_same_server = exists S. lsn(S) & ssemaphore(S) & forall S2. (S2 ~= S) -> (lsb(S2) & sn(S2))
# relation two_links_diff_server = two_links_same_server
# relation three_links_same_server = forall S. lsb(S) & sn(S)
# relation three_links_half_server = three_links_same_server
# relation three_links_diff_server = three_links_same_server
# 
# invariant [r]
# 	  (no_links_client & no_links_server)
# 	| (one_link_client & one_link_server)
# 	| (two_links_same_client & two_links_same_server)
# 	| (two_links_diff_client & two_links_diff_server)
# 	| (three_links_same_client & three_links_same_server)
# 	| (three_links_half_client & three_links_half_server)
# 	| (three_links_diff_client & three_links_diff_server)

relation no_links = forall C, S. ~link(C,S) & semaphore(S)
relation one_link = exists C0, S0. link(C0,S0) & ~semaphore(S0) & forall C, S. ((S ~= S0) -> semaphore(S)) & ((C ~= C0 | S ~= S0) -> ~link(C,S))
relation two_links_same = exists C0, S0, S1. (S0 ~= S1) & link(C0,S0) & link(C0,S1) & ~semaphore(S0) & ~semaphore(S1) & forall C, S. ((S ~= S0 & S ~= S1) -> semaphore(S)) & (((C ~= C0 | S ~= S0) & (C ~= C0 | S ~= S1)) -> ~link(C,S))
relation two_links_diff = exists C0, C1, S0, S1. (C0 ~= C1 & S0 ~= S1) & link(C0,S1) & link(C1,S0) & ~semaphore(S0) & ~semaphore(S1) & forall C, S. ((S ~= S0 & S ~= S1) -> semaphore(S)) & (((C ~= C0 | S ~= S1) & (C ~= C1 | S ~= S0)) -> ~link(C,S))
relation three_links_same = exists C0, S0, S1, S2. (S0 ~= S1 & S0 ~= S2 & S1 ~= S2) & link(C0,S0) & link(C0,S1) & link(C0,S2) & ~semaphore(S0) & ~semaphore(S1) & ~semaphore(S2) & forall C, S. ((S ~= S0 & S ~= S1 & S ~= S2) -> semaphore(S)) & ((C ~= C0 | S ~= S0) & ((C ~= C0 | S ~= S1) & (C ~= C0 | S ~= S2)) -> ~link(C,S))
relation three_links_half = exists C0, C1, S0, S1, S2. (C0 ~= C1 & S0 ~= S1 & S0 ~= S2 & S1 ~= S2) & link(C0,S1) & link(C0,S2) & link(C1,S0) & ~semaphore(S0) & ~semaphore(S1) & ~semaphore(S2) & forall C, S. ((S ~= S0 & S ~= S1 & S ~= S2) -> semaphore(S)) & (((C ~= C0 | S ~= S1) & (C ~= C0 | S ~= S2) & (C ~= C1 | S ~= S0)) -> ~link(C,S))
relation three_links_diff = exists C0, C1, C2, S0, S1, S2. (C0 ~= C1 & C0 ~= C2 & C1 ~= C2 & S0 ~= S1 & S0 ~= S2 & S1 ~= S2) & link(C0,S2) & link(C1,S1) & link(C2,S0) & ~semaphore(S0) & ~semaphore(S1) & ~semaphore(S2) & forall C, S. ((S ~= S0 & S ~= S1 & S ~= S2) -> semaphore(S)) & (((C ~= C0 | S ~= S2) & (C ~= C1 | S ~= S1) & (C ~= C2 | S ~= S0)) -> ~link(C,S))

invariant [r]
	  no_links
	| one_link
	| two_links_same
	| two_links_diff
	| three_links_same
	| three_links_half
	| three_links_diff

# invariant [x] (no_links | one_link | two_links_same | two_links_same | two_links_diff | three_links_same | three_links_half | three_links_diff) -> unique
