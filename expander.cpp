#include <vector>
#include <string>
#include <set>
#include <algorithm>
#include <iostream>

using namespace std;



struct Mapping {
	string from, to;
};



class Index_Symmetry {
public:
	Index_Symmetry(const string &indices);
	bool next_permutation();
	vector<Mapping> get_permutation() const;

private:
	string i_0, i_1, i_2;
	size_t permutation;
};



class Expander {
public:
	Expander();
	void parse_symmetries(istream &input);
	void parse_literals(istream &input);
	void parse_cubes(istream &input);
	void create_literal_symmetries();
	void expand_cubes();

private:
	vector<string> cubes;
	vector<vector<size_t>> literal_symmetries;
	vector<Index_Symmetry> index_symmetries;
	vector<string> literals;

	string apply_symmetry(size_t c, size_t ls) const;
	void print_cube(size_t c) const;
};



int main(int argc, char **argv) {
	if (argc != 1) {
		cerr << "usage: " << argv[0] << '\n';
		return 1;
	}
	Expander expander;
	expander.parse_symmetries(cin);
	expander.parse_literals(cin);
	expander.parse_cubes(cin);
	expander.create_literal_symmetries();
	expander.expand_cubes();
	return 0;
}



string replace(const string &str, const vector<Mapping> &rules) {
	string result = str;
	size_t i = 0;
	while (i < result.size()) {
		for (Mapping m : rules) {
			if (result.substr(i, m.from.size()) == m.from) {
				result.replace(i, m.from.size(), m.to);
				i += m.to.size();
				break;
			}
		}
		++i;
	}
	return result;
}



Index_Symmetry::Index_Symmetry(const string &indices)
: permutation{0} {
	size_t space_0 = indices.find(" ");
	size_t space_1 = indices.find(" ", space_0 + 1);
	i_0 = indices.substr(0, space_0);
	i_1 = indices.substr(space_0 + 1, space_1 - space_0 - 1);
	i_2 = indices.substr(space_1 + 1);
}

bool Index_Symmetry::next_permutation() {
	permutation = (permutation + 1) % 6;
	return permutation;
}

vector<Mapping> Index_Symmetry::get_permutation() const {
	Mapping m_0, m_1, m_2;
	m_0.from = i_0;
	m_1.from = i_1;
	m_2.from = i_2;
	switch (permutation) {
		case 0:
			m_0.to = i_0;
			m_1.to = i_1;
			m_2.to = i_2;
			break;
		case 1:
			m_0.to = i_0;
			m_1.to = i_2;
			m_2.to = i_1;
			break;
		case 2:
			m_0.to = i_1;
			m_1.to = i_0;
			m_2.to = i_2;
			break;
		case 3:
			m_0.to = i_1;
			m_1.to = i_2;
			m_2.to = i_0;
			break;
		case 4:
			m_0.to = i_2;
			m_1.to = i_0;
			m_2.to = i_1;
			break;
		case 5:
			m_0.to = i_2;
			m_1.to = i_1;
			m_2.to = i_0;
			break;
	}
	return {m_0, m_1, m_2};
}



Expander::Expander() {}

void Expander::parse_symmetries(istream &input) {
	string line;
	while (getline(input, line)) {
		if (!line.size()) continue;
		index_symmetries.emplace_back(line);
		if (index_symmetries.size() == 2) break;
	}
}

void Expander::parse_literals(istream &input) {
	string line;
	while (getline(input, line)) {
		if (!line.size()) continue;
		line += " ";
		while (line.size()) {
			size_t space = line.find(" ");
			literals.push_back(line.substr(0, space));
			line.erase(0, space + 1);
		}
		break;
	}
}

void Expander::parse_cubes(istream &input) {
	string line;
	while (getline(input, line)) {
		if (!line.size()) continue;
		line.erase(remove(line.begin(), line.end(), ' '), line.end());
		if (line.size() != literals.size() + 1) {
			cerr << "check cubes\n";
			exit(1);
		}
		line.resize(literals.size());
		cubes.push_back(line);
	}
}

void Expander::create_literal_symmetries() {
	do {
		vector<Mapping> mapping_0 = index_symmetries[0].get_permutation();
		do {
			vector<Mapping> mapping_1 = index_symmetries[1].get_permutation();
			vector<size_t> literal_symmetry;
			for (size_t l = 0; l < literals.size(); ++l) {
				string replacement = replace(literals[l], mapping_0);
				replacement = replace(replacement, mapping_1);
				auto pos = find(literals.begin(), literals.end(), replacement);
				if (pos == literals.end()) {
					cerr << "check symmetries and literals\n";
					exit(1);
				}
				literal_symmetry.push_back(pos - literals.begin());
			}
			literal_symmetries.push_back(literal_symmetry);
		} while (index_symmetries[1].next_permutation());
	} while (index_symmetries[0].next_permutation());
}

void Expander::expand_cubes() {
	set<string> expansion;
	for (size_t c = 0; c < cubes.size(); ++c) {
		for (size_t ls = 0; ls < literal_symmetries.size(); ++ls) {
			expansion.insert(apply_symmetry(c, ls));
		}
	}
	for (const string &cube : expansion) {
		cout << cube << " 1\n";
	}
}

string Expander::apply_symmetry(size_t c, size_t ls) const {
	const string &cube = cubes[c];
	const vector<size_t> &literal_symmetry = literal_symmetries[ls];
	string result;
	result.resize(cube.size(), ' ');
	for (size_t i = 0; i < literal_symmetry.size(); ++i) {
		result[literal_symmetry[i]] = cube[i];
	}
	return result;
}
