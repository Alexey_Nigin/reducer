CXXFLAGS = -Wconversion -Wall -Werror -Wextra -pedantic -g3 -DDEBUG

all: reducer expander
reducer: reducer.cpp
	g++ -std=c++1z ${CXXFLAGS} reducer.cpp -o reducer
expander: expander.cpp
	g++ -std=c++1z ${CXXFLAGS} expander.cpp -o expander
clean:
	rm -f reducer expander
.PHONY: all clean
